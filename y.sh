#!/bin/bash
#replace vowels with a given letter (y by default)

char='y'
string=$@

case $1 in
	-char=*) 
		char=$(echo $1 | sed 's/-char=//')
		string=$(echo $@ | sed -e 's/^-char=[^ ]*\s*//')
		;;
esac
charl=$(echo $char | tr '[:upper:]' '[:lower:]')
charu=$(echo $char | tr '[:lower:]' '[:upper:]')

echo $string | tr "aeiou" $charl | tr "AEIOU" $charu
