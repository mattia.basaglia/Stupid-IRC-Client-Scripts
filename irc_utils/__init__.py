import sys

irc_colors = {
	"black"       : 0,
	"white"       : 1,
	"dark_blue"   : 2,
	"dark_green"  : 3,
	"red"         : 4,
	"dark_red"    : 5,
	"dark_magenta": 6,
	"dark_yellow" : 7,
	"yellow"      : 8,
	"green"       : 9,
	"dark_cyan"   : 10,
	"cyan"        : 11,
	"blue"        : 12,
	"magenta"     : 13,
	"dark_gray"   : 14,
	"gray"        : 15
}

def irc_function(func):
	 print(func(' '.join(sys.argv[1:])))